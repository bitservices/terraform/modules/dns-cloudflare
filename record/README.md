<!---------------------------------------------------------------------------->

# record

#### Manage [Cloudflare] DNS records within a DNS zone

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/dns/cloudflare//record`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_cloudflare_record" {
  source = "gitlab.com/bitservices/dns/cloudflare//record"
  name   = "foobar"
  zone   = "bitservices.io"
  value  = "ef2a5d67.bitservices.io"
}
```

<!---------------------------------------------------------------------------->

[Cloudflare]: https://www.cloudflare.com/

<!---------------------------------------------------------------------------->
