###############################################################################
# Required Variables
###############################################################################

variable "zone" {
  type        = string
  description = "The DNS zone ID (if 'zone_lookup' is 'false') or name (if 'zone_lookup' is 'true')."
}

###############################################################################
# Optional Variables
###############################################################################

variable "zone_lookup" {
  type        = string
  default     = true
  description = "Lookup the DNS zone ID from its name."
}

###############################################################################
# Locals
###############################################################################

locals {
  zone_id = var.zone_lookup ? data.cloudflare_zone.object[0].id : var.zone
}

###############################################################################
# Data Sources
###############################################################################

data "cloudflare_zone" "object" {
  count = var.zone_lookup ? 1 : 0
  name  = var.zone
}

###############################################################################
# Outputs
###############################################################################

output "zone_lookup" {
  value = var.zone_lookup
}

###############################################################################

output "zone_id" {
  value = local.zone_id
}

###############################################################################

output "zone_name" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].name : null
}

output "zone_plan" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].plan : null
}

output "zone_paused" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].paused : null
}

output "zone_status" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].status : null
}

output "zone_account_id" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].account_id : null
}

output "zone_name_servers" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].name_servers : null
}

output "zone_vanity_name_servers" {
  value = length(data.cloudflare_zone.object) == 1 ? data.cloudflare_zone.object[0].vanity_name_servers : null
}

###############################################################################
