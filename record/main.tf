###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "Name of the DNS record within the zone. The zone name is automatically appended."
}

variable "value" {
  type        = string
  description = "The value of the DNS record."
}

###############################################################################
# Optional Variables
###############################################################################

variable "ttl" {
  type        = number
  default     = 300
  description = "DNS record Time-To-Live in seconds."
}

variable "type" {
  type        = string
  default     = "CNAME"
  description = "The DNS record type."
}

variable "proxied" {
  type        = bool
  default     = false
  description = "Should the CloudFlare CDN service be enabled for this DNS record."
}

###############################################################################
# Locals
###############################################################################

locals {
  ttl     = local.proxied ? 1 : var.ttl
  proxied = contains([ "A", "AAAA", "CNAME" ], var.type) ? var.proxied : false
}

###############################################################################
# Resources
###############################################################################

resource "cloudflare_record" "object" {
  ttl     = local.ttl
  name    = var.name
  type    = var.type
  value   = var.value
  proxied = local.proxied
  zone_id = local.zone_id
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = cloudflare_record.object.id
}

output "fqdn" {
  value = cloudflare_record.object.hostname
}

output "created" {
  value = cloudflare_record.object.created_on
}

output "metadata" {
  value = cloudflare_record.object.metadata
}

output "modified" {
  value = cloudflare_record.object.modified_on
}

output "proxiable" {
  value = cloudflare_record.object.proxiable
}

###############################################################################
